using Playroom;
using UnityEngine;

namespace Aurea.NetLayer.Sample
{
    public class LocalPlayerInputProvider : MonoBehaviour, IInputProvider
    {
        public float Horizontal => Input.GetAxis("Horizontal");

        public float Vertical => Input.GetAxis("Vertical");

        public bool Kicking => false;


        private void Update()
        {
            if (PlayroomKit.IsHost())
            {
                if (Input.GetKeyDown(KeyCode.B))
                {
                    NetworkManager.Instance.SpawnObject(0, (Random.onUnitSphere * 3) + Vector3.up * 3, Quaternion.identity, false);
                }
                if (Input.GetKeyDown(KeyCode.N))
                {
                    Ball aBall = FindObjectOfType<Ball>();
                    if (aBall != null)
                    {
                        NetworkManager.Instance.UnspawnObject(aBall.GetComponent<NetworkObjectBase>().Id, false);
                    }
                }
            }

            //  instead of passing Kicking input, we call the kick method via RPC
            if (Input.GetKeyDown(KeyCode.Space))
            {
                NetworkManager.Instance.Rpc(GetComponent<NetworkObjectBase>(), "Kick", null, true);
            }
        }
    }
}