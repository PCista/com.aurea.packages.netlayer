using UnityEngine;
using Aurea.NetLayer;

namespace Aurea.NetLayer.Sample
{
    public class PlayerConfig
    {
        public string PlayerId = "";
        public bool IsLocal = true;
        public Color Color = Color.yellow;
        public int PlayerIndex = -1;
    }

    public class PlayerSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject _localPlayerPrefab = null;
        [SerializeField] private GameObject _remotePlayerPrefab = null;

        public PlayerControllerBase PlayerController { get; private set; } = null;
        public bool PlayerIsSpawned => PlayerController != null;

        private void OnDestroy()
        {
            Deinitialize();
        }

        public void Deinitialize()
        {
            Unspawn();
        }

        public PlayerControllerBase Spawn(PlayerConfig config)
        {
            if (PlayerController != null)
            {
                Debug.LogWarning("Player is already spawned.");
                return PlayerController;
            }

            if (config == null)
            {
                Debug.Log("Unable to Spawn Player. PlayerConfig is not set.");
                return null;
            }

            GameObject go = Instantiate(config.IsLocal ? _localPlayerPrefab : _remotePlayerPrefab, transform.position, transform.rotation);
            PlayerController = go.GetComponent<PlayerControllerBase>();
            PlayerController.Initialize(config);

            if (!NetworkManager.IsInitialized)
            {
                Destroy(go.GetComponent<NetworkObjectBase>());
            }
            else
            {
                go.GetComponent<NetworkObjectBase>().Initialize("Player_" + config.PlayerId, !config.IsLocal);
            }

            return PlayerController;
        }

        public void Unspawn()
        {
            if (PlayerController != null)
            {
                Destroy(PlayerController.gameObject);
                PlayerController = null;
            }
        }
    }
}