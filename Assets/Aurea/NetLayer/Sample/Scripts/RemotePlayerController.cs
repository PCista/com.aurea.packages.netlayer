using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea.NetLayer.Sample
{
    public class RemotePlayerController : PlayerControllerBase
    {
        private void Awake()
        {
            _characterController.enabled = false;
        }

        private void Update()
        {

        }

        public override void Initialize(PlayerConfig config)
        {
            base.Initialize(config);

            _inputProvider = gameObject.AddComponent<RemotePlayerInputProvider>();
            Debug.Log("Initialized " + config.PlayerId + " as Remote Player");
        }

        public override void Deinitialize()
        {
            base.Deinitialize();
        }
    }
}