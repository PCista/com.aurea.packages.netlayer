using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea.NetLayer.Sample
{

    public class GameManager : MonoBehaviour
    {
        [SerializeField] private List<PlayerSpawner> _playerSpawners;

        private List<PlayerConfig> _playerConfigs = new List<PlayerConfig>();

        private bool _gameHasStarted = false;


        private void Awake()
        {
            NetworkManager.Instance.OnPlayerJoin += OnPlayerJoin;
            NetworkManager.Instance.OnPlayerLeave += OnPlayerLeave;
            NetworkManager.Instance.OnStartGame += OnStartGame;
        }

        private void Start()
        {
            Application.targetFrameRate = 60;

            NetworkManager.Instance.Initialize(Utils.GenerateRoomCode());
        }

        private void OnPlayerJoin(Aurea.NetLayer.NetworkPlayer netPlayer)
        {
            if (_gameHasStarted)
                return;

            Debug.Log($"{netPlayer.Id} joined!");

            PlayerConfig config = new PlayerConfig();
            config.Color = netPlayer.Color;
            config.IsLocal = netPlayer.IsLocal;
            config.PlayerId = netPlayer.Id;
            config.PlayerIndex = netPlayer.Index;   //  At this point, users that are not Host still don't know the index of the players. We need to wait until StartGame is called to get that.
            _playerConfigs.Add(config);

            if (_playerConfigs.Count == 1 && NetworkManager.Instance.IsHost())
            {
                StopAllCoroutines();
                StartCoroutine(WaitAndStartGame());
            }
        }

        private void OnPlayerLeave(Aurea.NetLayer.NetworkPlayer player)
        {
            string id = player.Id;
            Debug.Log($"{id} quit!");

            PlayerConfig config = _playerConfigs.Find(x => x.PlayerId == player.Id);
            PlayerSpawner spawner = _playerSpawners[config.PlayerIndex];
            spawner.Deinitialize();

            _playerConfigs.Remove(config);
        }

        private void OnStartGame(Aurea.NetLayer.StartGameMessage message)
        {

            foreach (PlayerConfig config in _playerConfigs)
            {
                //  Fill PlayerIndex data into PlayerConfigs
                config.PlayerIndex = NetworkManager.Instance.GetPlayer(config.PlayerId).Index;

                //  Spawn the player
                PlayerSpawner playerSpawner = _playerSpawners[config.PlayerIndex];
                playerSpawner.Spawn(config);
            }
        }

        private IEnumerator WaitAndStartGame()
        {
            //  Wait 2s to other players join the game
            yield return new WaitForSeconds(2f);

            //  Start the game
            NetworkManager.Instance.StartGame();
        }
    }
}