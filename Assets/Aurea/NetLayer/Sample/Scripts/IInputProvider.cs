using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea.NetLayer.Sample
{
    public interface IInputProvider
    {
        float Horizontal { get; }
        float Vertical { get; }

        bool Kicking { get; }
    }
}