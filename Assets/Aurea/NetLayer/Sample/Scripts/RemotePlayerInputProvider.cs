using UnityEngine;

namespace Aurea.NetLayer.Sample
{

    public class RemotePlayerInputProvider : MonoBehaviour, IInputProvider
    {
        public float Horizontal => _horizontal;

        public float Vertical => _vertical;

        public bool Kicking => _kicking;

        private float _horizontal = 0;
        private float _vertical = 0;
        private bool _kicking = false;
    }
}