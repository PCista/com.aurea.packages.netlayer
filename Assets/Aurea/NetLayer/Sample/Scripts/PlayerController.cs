using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea.NetLayer.Sample
{

    public class PlayerController : PlayerControllerBase
    {

        private void Update()
        {
            if (_inputProvider == null)
                return;

            Vector3 move = new Vector3(_inputProvider.Horizontal, 0, _inputProvider.Vertical).normalized * _speed * Time.deltaTime;
            _characterController.Move(move);

            if (move != Vector3.zero)
                _lastMoveDirection = move;

            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(_lastMoveDirection), RotationSpeed * Time.deltaTime);


            if (Input.Kicking)
            {
                Kick();
            }
        }

        public override void Initialize(PlayerConfig config)
        {
            base.Initialize(config);

            _inputProvider = gameObject.AddComponent<LocalPlayerInputProvider>();
            Debug.Log("Initialized " + config.PlayerId + " as Local Player");

        }

        public override void Deinitialize()
        {

        }
    }
}