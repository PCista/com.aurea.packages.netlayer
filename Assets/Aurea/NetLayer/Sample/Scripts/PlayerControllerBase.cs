using UnityEngine;

namespace Aurea.NetLayer.Sample
{
    public abstract class PlayerControllerBase : MonoBehaviour
    {
        [SerializeField] protected CharacterController _characterController;
        [SerializeField] protected float _speed = 7;
        [SerializeField] protected MeshRenderer _meshRenderer;

        protected IInputProvider _inputProvider = null;
        protected Vector3 _lastMoveDirection = Vector3.zero;

        public IInputProvider Input => _inputProvider;

        public float Speed => _speed;
        public float RotationSpeed => 360 * 2;
        public string PlayerId { get; private set; } = "";


        private void Start()
        {
            _lastMoveDirection = transform.forward;
        }

        public virtual void Initialize(PlayerConfig config)
        {
            PlayerId = config.PlayerId;

            Material mat = new Material(_meshRenderer.material);
            mat.color = config.Color;
            _meshRenderer.material = mat;
        }

        public virtual void Deinitialize()
        {

        }

        public void Kick()
        {
            RaycastHit[] raycastHits = Physics.SphereCastAll(transform.position + transform.forward, 0.5f, transform.forward, 0f);
            foreach (RaycastHit hit in raycastHits)
            {
                if (hit.collider.tag == "Ball")
                {
                    hit.rigidbody.AddForce((transform.forward + Vector3.up * 0.5f) * 10, ForceMode.Impulse);
                }
            }
        }
    }
}