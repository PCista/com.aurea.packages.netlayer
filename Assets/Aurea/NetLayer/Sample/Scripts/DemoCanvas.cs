using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Aurea.NetLayer;

namespace Aurea.NetLayer.Sample
{

    public class DemoCanvas : MonoBehaviour
    {
        [SerializeField] private GameObject _waitingOverlay;
        [SerializeField] private GameObject _controls;

        private void Awake()
        {
            NetworkManager.Instance.OnStartGame += OnStartGame;

            _waitingOverlay.SetActive(true);
            _controls.SetActive(false);
        }

        private void OnStartGame(Aurea.NetLayer.StartGameMessage message)
        {
            _waitingOverlay.SetActive(false);
            _controls.SetActive(true);
        }
    }
}