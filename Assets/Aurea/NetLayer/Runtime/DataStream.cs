using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {
        public class DataStream
        {
            private List<object> _dataArray = new List<object>();
            private int _readIndex = 0;

            public bool IsEmpty => _dataArray.Count == 0;

            public DataStream()
            {

            }

            public void Clear()
            {
                _dataArray.Clear();
                _readIndex = 0;
            }

            public void Write(object data)
            {
                if (data is Vector3)
                {
                    Vector3 v = (Vector3)data;
                    Write(v.x);
                    Write(v.y);
                    Write(v.z);
                }
                else if (data is float)
                {
                    _dataArray.Add(Utils.Quantize((float)data));
                }
                else if (data is bool)
                {
                    _dataArray.Add((bool)data ? 1 : 0);
                }
                else if (data is Color)
                {
                    Color c = (Color)data;
                    Write((int)(c.r * 255));
                    Write((int)(c.g * 255));
                    Write((int)(c.b * 255));
                    Write((int)(c.a * 255));
                }
                else
                {
                    _dataArray.Add(data);
                }
            }

            public bool ReadBool()
            {
                try
                {
                    return ReadInt() != 0;
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    return false;
                }
            }

            public int ReadInt()
            {
                try
                {
                    _readIndex++;
                    return Convert.ToInt32(_dataArray[_readIndex - 1]);
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    return 0;
                }
            }

            public float ReadFloat()
            {
                try
                {
                    _readIndex++;
                    return Utils.Dequantize(Convert.ToInt32(_dataArray[_readIndex-1]));
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    return 0f;
                }
            }

            public Vector3 ReadVector3()
            {
                try
                {
                    return new Vector3(
                            ReadFloat(),
                            ReadFloat(),
                            ReadFloat()
                            );
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    return Vector3.zero;
                }
            }

            public Color ReadColor()
            {
                try
                {
                    return new Color(
                            ReadInt() / 255f,
                            ReadInt() / 255f,
                            ReadInt() / 255f,
                            ReadInt() / 255f
                            );
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    return Color.black;
                }
            }

            public string ReadString()
            {
                try
                {
                    _readIndex++;
                    return (string)_dataArray[_readIndex - 1];
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    return string.Empty;
                }
            }

            public T Read<T>()
            {
                try
                {
                    _readIndex++;
                    return (T)_dataArray[_readIndex - 1];
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    return default(T);
                }
            }

            public ObjectDataArray Encode()
            {
                ObjectDataArray data = new ObjectDataArray();
                data.Array = _dataArray.ToArray();
                data.Length = _dataArray.Count;
                return data;
            }

            public void Decode(ObjectDataArray dataArray)
            {
                if (dataArray == null)
                {
                    Debug.LogError("DataArray is null!");
                    return;
                }

                _dataArray.AddRange(dataArray.Array);
            }
        }

        [System.Serializable]
        public class ObjectDataArray
        {
            public object[] Array;
            public int Length;

            public bool Equals(ObjectDataArray other)
            {
                if (Length != other.Length)
                    return false;

                for (int i = 0; i < Length; ++i)
                {
                    if (Array[i] != other.Array[i])
                        return false;
                }

                return true;
            }
        }
    }
}