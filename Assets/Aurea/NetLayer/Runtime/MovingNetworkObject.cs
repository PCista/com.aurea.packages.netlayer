using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {
        [System.Serializable]
        public class MovingNetworkObjectState: NetworkState
        {
            public Vector3 Position;
            public Vector3 Rotation;
        }

        public class MovingNetworkObject : NetworkObjectBase
        {
            [SerializeField]
            protected bool _interpolate = true;

            [SerializeField]
            protected float _interpolationMaxSpeed = 7;

            [SerializeField]
            protected float _interpolationMaxRotationSpeed = 720;

            protected MovingNetworkObjectState _targetState;

            private void Awake()
            {
                _targetState = new MovingNetworkObjectState();
                _targetState = GetState() as MovingNetworkObjectState;

                _currentState = new MovingNetworkObjectState();
                _currentState = GetState();
            }

            protected virtual void Update()
            {
                if (!NetworkManager.IsInitialized)
                    return;

                if (string.IsNullOrEmpty(Id))
                    return;

                if (IsOwnedByMe())
                    return;

                if (_interpolate)
                {
                    transform.position = Vector3.MoveTowards(transform.position, _targetState.Position, Time.deltaTime * _interpolationMaxSpeed);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(_targetState.Rotation), Time.deltaTime * _interpolationMaxRotationSpeed);
                }
            }

            public override NetworkState GetState()
            {
                MovingNetworkObjectState state = _currentState as MovingNetworkObjectState;
                state.Position = transform.position;
                state.Rotation = transform.rotation.eulerAngles;
                return state;
            }

            public override void SetState(NetworkState state)
            {
                _targetState = state as MovingNetworkObjectState;

                if (!_interpolate)
                {
                    transform.position = _targetState.Position;
                    transform.rotation = Quaternion.Euler(_targetState.Rotation);
                }
            }

            public override Type GetStateType()
            {
                return typeof(MovingNetworkObjectState);
            }
        }
    }
}