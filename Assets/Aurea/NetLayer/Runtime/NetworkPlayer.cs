using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {
        public class NetworkPlayer
        {
            public string Id;
            public int Index;
            public bool IsLocal = false;
            public Color Color = Color.white;
        }
    }
}