using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {
        public static class Utils
        {
            private static System.Random random = new System.Random();

            public static bool IsNan(this Vector3 vector3)
            {
                return float.IsNaN(vector3.x) || float.IsNaN(vector3.y) || float.IsNaN(vector3.z);
            }

            public static bool IsNan(this Quaternion quaternion)
            {
                return float.IsNaN(quaternion.x) || float.IsNaN(quaternion.y) || float.IsNaN(quaternion.z) || float.IsNaN(quaternion.w);
            }

            public static bool LessEqual(float a, float b)
            {
                return a < b || Mathf.Abs(a - b) < float.Epsilon;
            }

            public static string GetGameObjectPath(GameObject obj)
            {
                string path = "/" + obj.name;
                while (obj.transform.parent != null)
                {
                    obj = obj.transform.parent.gameObject;
                    path = "/" + obj.name + path;
                }
                return path;
            }

            public static string GenerateRoomCode()
            {
                const string allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                StringBuilder roomCodeBuilder = new StringBuilder();

                for (int i = 0; i < 4; i++)
                {
                    int randomIndex = random.Next(0, allowedCharacters.Length);
                    roomCodeBuilder.Append(allowedCharacters[randomIndex]);
                }

                return roomCodeBuilder.ToString();
            }

            public static int Quantize(float value)
            {
                return (int)(value * 1000f);
            }

            public static float Dequantize(int value)
            {
                return value / 1000f;
            }

            public static Vector3Int QuantizeVector3(Vector3 value)
            {
                return new Vector3Int(
                    Quantize(value.x),
                    Quantize(value.z),
                    Quantize(value.z)
                    );
            }

            public static Vector3 DequantizeVector3(Vector3Int value)
            {
                return new Vector3(
                    Dequantize(value.x),
                    Dequantize(value.y),
                    Dequantize(value.z)
                    );
            }
        }
    }
}