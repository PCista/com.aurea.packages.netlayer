
namespace Aurea
{
    namespace NetLayer
    {
        [System.Serializable]
        public enum NetworkOwner
        {
            Host,
            LocalPlayer,
            Other
        }
    }
}