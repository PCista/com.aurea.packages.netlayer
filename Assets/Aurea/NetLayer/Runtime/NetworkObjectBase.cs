using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {
        public abstract class NetworkObjectBase : MonoBehaviour
        {
            [SerializeField]
            private NetworkOwner _owner;

            protected NetworkState _currentState = new NetworkState();

            private string _id = string.Empty;

            private DataStream _dataStream = new DataStream();

            public NetworkOwner Owner => _owner;

            public string Id => _id;

            /// <summary>
            /// The DataStream used by this object. This is supposed to be used only by the NetworkManager. Don't mess with it.
            /// </summary>
            public DataStream DataStream => _dataStream;

            /// <summary>
            /// Whether this Network Object's state class overrides the Serialize and Deserialize methods or not. 
            /// Serialization method can be implemented to provide smalled network messages. Improving network speed.
            /// NOTE: This is not supposed to change during runtime. Set it once in the Awake method, then let it be.
            /// </summary>
            public bool EnabledDataStream { get; protected set; } = false;

            protected virtual void Start()
            {
                if (string.IsNullOrEmpty(_id))
                {
                    StartCoroutine(WaitNetworkInitializationToSync());
                }
                else
                {
                    if (NetworkManager.IsInitialized)
                        NetworkManager.Instance.AddNetworkObject(this);
                }
            }

            protected virtual void OnDestroy()
            {
                if (string.IsNullOrEmpty(_id))
                {
                    if (NetworkManager.IsInitialized)
                        NetworkManager.Instance.RemoveUnidentifiedNetworkObject(this);
                }
                else
                {
                    if (NetworkManager.IsInitialized)
                        NetworkManager.Instance.RemoveNetworkObject(this);
                }
            }

            public void Initialize(string id, bool ownedByOther)
            {
                _id = id;
                if (ownedByOther)
                    _owner = NetworkOwner.Other;
            }

            public abstract NetworkState GetState();

            public abstract void SetState(NetworkState state);

            public abstract Type GetStateType();

            public bool IsOwnedByMe()
            {
                return Owner == NetworkOwner.LocalPlayer || (Owner == NetworkOwner.Host && NetworkManager.Instance.IsHost());
            }

            private IEnumerator WaitNetworkInitializationToSync()
            {
                yield return new WaitUntil(() => NetworkManager.IsInitialized);

                NetworkManager.Instance.AddUnidentifiedNetworkObject(this);
            }
        }
    }
}