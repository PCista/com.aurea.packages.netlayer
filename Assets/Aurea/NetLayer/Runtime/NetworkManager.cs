using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Playroom;
using UnityEngine.Events;
using System;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

namespace Aurea
{
    namespace NetLayer
    {
        public class NetworkManager : MonoBehaviour
        {
            /// <summary>
            /// Helper class to keep track of a enqueued message and its receiver
            /// </summary>
            public class NetworkMessageQueueEntry
            {
                public NetworkMessage Message;
                public string ReceiverId;

                public NetworkMessageQueueEntry(NetworkMessage message, string receiver)
                {
                    Message = message;
                    ReceiverId = receiver;
                }
            }

            private static NetworkManager _instance = null;

            public static NetworkManager Instance
            {
                get
                {
                    if (_instance == null)
                    {
                        GameObject prefab = Resources.Load<GameObject>("NetworkManager");
                        _instance = Instantiate(prefab).GetComponent<NetworkManager>();

                        if (_instance == null)
                        {
                            GameObject go = new GameObject("NetworkManager");
                            _instance = go.AddComponent<NetworkManager>();
                        }
                    }
                    return _instance;
                }
            }

            public static bool IsInitialized { get; set; } = false;

            public static float Time
            {
                get
                {
                    if (IsInitialized)
                        return UnityEngine.Time.unscaledTime - Instance._startTime;
                    else
                        return 0;
                }
            }


            public UnityAction OnStartRoom;
            public UnityAction<NetworkPlayer> OnPlayerJoin;
            public UnityAction<NetworkPlayer> OnPlayerLeave;
            public UnityAction<StartGameMessage> OnStartGame;
            public UnityAction<NetworkMessage> OnReceiveNetworkMessage;

            [SerializeField] private List<GameObject> _prefabs = new List<GameObject>();

            [SerializeField] private bool _showPlayroomLobby = true;
            [SerializeField] private string _gameId = "";
            [SerializeField] private int _maxPlayersPerRoom = 4;
            [Tooltip("How many updates the host sends to other players each second. -1 means one per frame. [NOTE] Currently it does not work as expected due to WebRPC issues, so leave it to -1 to better results. But feel free to test it out")]
            [SerializeField] private int _updatesPerSecond = -1;

            private List<NetworkPlayer> _players = new List<NetworkPlayer>();

            private List<NetworkObjectBase> _networkObjects = new List<NetworkObjectBase>();
            private Dictionary<string, NetworkObjectBase> _unidentifiedNetworkObjects = new Dictionary<string, NetworkObjectBase>();    //  Key = object id, Value = the object

            private Queue<NetworkMessageQueueEntry> _messageQueue = new Queue<NetworkMessageQueueEntry>();
            private Dictionary<string, NetworkMessageList> _reliableMessages = new Dictionary<string, NetworkMessageList>();    //  Key = ReceiverId, Value = MessagesToReceive
            private Dictionary<string, int> _lastAckedMessageId = new Dictionary<string, int>();                                //  Key = SenderId (who sent the message to me), Value = LastReceivedMessageId

            private float _startTime = 0;
            private string _roomCode = "";

            private const string MESSAGE_KEY = "__message.";
            private const string ACK_MESSAGE_KEY = "__ack.";
            private const string SPAWN_MESSAGE_NAME = "__SpawnObject";
            private const string UNSPAWN_MESSAGE_NAME = "__UnspawnObject";
            private const string RPC_MESSAGE_NAME = "__Rpc";
            private const string KICK_MESSAGE_NAME = "__Kick";
            private const string STARTGAME_MESSAGE_NAME = "__StartGame";
            private const string ROOM_LOCK_NAME = "__roomLock.";

            private bool _alreadyAddedPlayerJoinCallback = false;

            private bool _isDisconnecting = false;

            private float _deltaTimeAccumulated = 0f;

            public bool IsRoomLocked 
            {
                get 
                {
                    if (!IsInitialized)
                        return false;
                    
                    //if (!IsHost())
                    //    return false;

                    string stateName = ROOM_LOCK_NAME + GetRoomCode();
                    return PlayroomKit.GetStateBool(stateName);
                }
                set 
                {
                    if (!IsInitialized)
                        return;

                    if (!IsHost())
                        return;

                    string stateName = ROOM_LOCK_NAME + GetRoomCode();
                    PlayroomKit.SetState(stateName, value);
                }
            }

            private void Awake()
            {
                if (_instance != null)
                {
                    Destroy(gameObject);
                    return;
                }

                _instance = this;
            }

            private void OnDestroy()
            {
                if (this == _instance)
                    IsInitialized = false;
            }

            private void Update()
            {
                if (!IsInitialized)
                    return;

                UpdateMessageQueue();

                EraseAckedNetworkMessages();
                CheckReceivedNetworkMessages();

                UpdateNetworkObjects();
            }

            public void Initialize(string roomCode = "R00M", bool matchmaking = false)
            {
                _roomCode = roomCode;
                _deltaTimeAccumulated = 0f;

                PlayroomKit.InitOptions initOptions = new PlayroomKit.InitOptions();
                initOptions.skipLobby = !_showPlayroomLobby;
                initOptions.maxPlayersPerRoom = _maxPlayersPerRoom;
                initOptions.roomCode = roomCode;
                initOptions.gameId = _gameId;
                initOptions.matchmaking = matchmaking;

                PlayroomKit.InsertCoin(initOptions, 
                    //  OnLaunchCallback
                    () =>
                    {
                        NetworkManager.IsInitialized = true;
                        _isDisconnecting = false;

                        SetStartTime(UnityEngine.Time.unscaledTime);

                        OnStartRoom?.Invoke();

                        PlayroomKit.OnPlayerJoin((player) =>
                        {
                            if (_players.Find(x => x.Id == player.id) != null)
                                return;

                            bool isLocal = player == PlayroomKit.MyPlayer();
                            if (IsHost() && isLocal)
                                IsRoomLocked = false;

                            if (IsRoomLocked)
                            {
                                Debug.Log("The room is locked!");
                                if (IsHost())
                                {
                                    Debug.Log("Kicking player " + player.id);
                                    KickPlayer(player.id);
                                }
                                PlayroomKit.Players.Remove(player.id);     //  NOTE:  This line is here because Playroom does not remove the player on OnQuit.

                                return;
                            }

                            Debug.Log("OnPlayerJoin " + player.id);

                            NetworkPlayer netPlayer = new NetworkPlayer();
                            netPlayer.Id = player.id;
                            netPlayer.Index = IsHost() ? _players.Count : -1;
                            netPlayer.IsLocal = player == PlayroomKit.MyPlayer();
                            netPlayer.Color = player.GetProfile().color;
                            _players.Add(netPlayer);
                            player.SetState(MESSAGE_KEY, "", true);

                            if (IsHost() && netPlayer.IsLocal)
                                IsRoomLocked = false;

                            OnPlayerJoin?.Invoke(netPlayer);

                            player.OnQuit((string id) =>
                            {
                                Debug.Log("OnQuit " + id);
                                NetworkPlayer quitPlayer = GetPlayer(id);
                                if (quitPlayer != null)
                                {
                                    ExitPlayer(id);
                                    //_players.Remove(quitPlayer);
                                    if(PlayroomKit.Players.ContainsKey(id))
                                        PlayroomKit.Players.Remove(id);     //  NOTE:  This line is here because Playroom does not remove the player on OnQuit.
                                }
                            });

                            
                        });
                        
                    },
                    //  OnDisconnectCallback
                    OnLocalPlayerDisconnect);

            }

            public void Deinitialize()
            {
                Debug.Log("Deinitialize");
                if (!IsInitialized)
                    return;

                if (!_isDisconnecting)
                {
                    if (IsHost())
                    {
                        Debug.Log("Kicking myself");
                        NetworkPlayer player = _players.Find(x => x.IsLocal);
                        if (player != null)
                        {
                            PlayroomKit.GetPlayer(player.Id)?.Kick();
                        }
                    }
                    else
                    {
                        Debug.Log("Requesting kick to myself");
                        NetworkPlayer player = _players.Find(x => x.IsLocal);
                        if (player != null)
                        {
                            KickPlayer(player.Id);
                            UpdateMessageQueue();
                        }
                    }
                }

                Debug.Log("Exiting players");
                for (int i = _players.Count -1; i >= 0; --i)
                {
                    NetworkPlayer player = _players[i];
                    Debug.Log("Exiting player - id: "+player.Id);
                    ExitPlayer(player.Id);
                }
                Debug.Log("Clear players");
                _players.Clear();

                Debug.Log("Clear NetworkObjects");
                foreach (NetworkObjectBase networkObject in _networkObjects)
                {
                    if (networkObject)
                    {
                        Debug.Log(networkObject.Id);
                        Destroy(networkObject.gameObject);
                    }
                }
                _networkObjects.Clear();

                Debug.Log("Clear Unidentified NetworkObjects");
                foreach (KeyValuePair<string, NetworkObjectBase> kvp in _unidentifiedNetworkObjects)
                {
                    if (kvp.Value)
                    {
                        Debug.Log(kvp.Value.Id);
                        Destroy(kvp.Value.gameObject);
                    }
                }
                _unidentifiedNetworkObjects.Clear();

                _messageQueue.Clear();
                _reliableMessages.Clear();
                _lastAckedMessageId.Clear();

                Debug.Log("NetworkManager.Initialized = false");
                NetworkManager.IsInitialized = false;
            }

            public void StartMatchmaking()
            {
                if(IsInitialized)
                    PlayroomKit.StartMatchmaking();
            }

            public bool IsHost()
            {
                if (!IsInitialized)
                    return false;

                return PlayroomKit.IsHost();
            }

            public void LockRoom()
            {
                IsRoomLocked = true;
            }

            public void UnlockRoom()
            {
                IsRoomLocked = false;
            }

            public string GetRoomCode()
            {
                if (IsInitialized)
                    return "";

#if UNITY_WEBGL && !UNITY_EDITOR
                return PlayroomKit.GetRoomCode();
#else
                return _roomCode;
#endif
            }

            public NetworkPlayer GetPlayer(string id)
            {
                return _players.Find(x => x.Id == id);
            }

            public List<NetworkPlayer> GetPlayers()
            {
                return _players;
            }

            public void KickPlayer(string playerId)
            {
                if (IsHost())
                {
                    NetworkMessage kickMessage = new NetworkMessage(KICK_MESSAGE_NAME, playerId, false);
                    HandleKickMessage(kickMessage);
                }
                else if (GetPlayer(playerId).IsLocal)
                {
                    NetworkMessage kickMessage = new NetworkMessage(KICK_MESSAGE_NAME, playerId, false);
                    BroadcastNetworkMessage(kickMessage, true);
                }
            }

            public void AddNetworkObject(NetworkObjectBase networkObject)
            {
                if (!_networkObjects.Contains(networkObject))
                {
                    _networkObjects.Add(networkObject);
                    Debug.Log("Added Network Object: " + networkObject.Id);
                }
            }

            public void AddUnidentifiedNetworkObject(NetworkObjectBase networkObject)
            {
                string key = Utils.GetGameObjectPath(networkObject.gameObject);
                _unidentifiedNetworkObjects.Add(key, networkObject);

                if (networkObject.Owner == NetworkOwner.Host || networkObject.Owner == NetworkOwner.LocalPlayer)
                {
                    SendSpawnObjectMessage(key, -1, networkObject.transform.position, networkObject.transform.rotation, false);
                }
            }

            public void RemoveUnidentifiedNetworkObject(NetworkObjectBase networkObject)
            {
                string key = Utils.GetGameObjectPath(networkObject.gameObject);
                _unidentifiedNetworkObjects.Remove(key);
            }

            public void RemoveNetworkObject(NetworkObjectBase networkObject)
            {
                Debug.Log("Removing network object: " + networkObject.Id);
                _networkObjects.Remove(networkObject);
            }

            public void RemoveNetworkObject(string id)
            {
                RemoveNetworkObject(_networkObjects.Find(x => x.Id == id));
            }

            public int FindPrefabIndex(string prefabName)
            {
                return _prefabs.FindIndex(x => x.name == prefabName);
            }

            public NetworkObjectBase SpawnObject(int prefabIndex, Vector3 position, Quaternion rotation, bool instantiateLocallyNow = true)
            {
                if (prefabIndex < 0 || prefabIndex >= _prefabs.Count)
                    return null;

                string id = System.Guid.NewGuid().ToString();

                if (instantiateLocallyNow)
                {
                    GameObject prefab = _prefabs[prefabIndex];
                    GameObject go = Instantiate(prefab, position, rotation);
                    NetworkObjectBase netObj = go.GetComponent<NetworkObjectBase>();

                    if (netObj != null)
                        netObj.Initialize(id, false);

                    SendSpawnObjectMessage(id, prefabIndex, position, rotation, instantiateLocallyNow);
                    return netObj;
                }

                SendSpawnObjectMessage(id, prefabIndex, position, rotation, instantiateLocallyNow);
                return null;
            }

            public void UnspawnObject(string id, bool exceptMyself)
            {
                NetworkObjectBase obj = _networkObjects.Find(x => x.Id == id);
                if (obj == null)
                {
                    Debug.LogWarning("Unable to Unspawn object '" + id + "'. Object not found!");
                    return;
                }

                if (!obj.IsOwnedByMe())
                {
                    Debug.LogWarning("Unable to Unspawn object '" + id + "'. You are not the owner of this object!");
                    return;
                }

                if (!exceptMyself)
                {
                    ClearNetworkObjectState(obj);
                    _networkObjects.Remove(obj);
                    Destroy(obj.gameObject);
                }

                SendUnspawnObjectMessage(id, exceptMyself);
            }

            public void Rpc(NetworkObjectBase networkObject, string methodName, object parameter = null, bool reliable = false)
            {
                if (networkObject == null)
                    return;

                if (!networkObject.IsOwnedByMe())
                    return;

                if (string.IsNullOrEmpty(networkObject.Id))
                    return;

                networkObject.gameObject.SendMessage(methodName, parameter, SendMessageOptions.DontRequireReceiver);

                RpcMessage rpc = new RpcMessage();
                rpc.ObjectId = networkObject.Id;
                rpc.MethodName = methodName;
                rpc.JsonData = JsonUtility.ToJson(parameter);

                NetworkMessage message = new NetworkMessage(RPC_MESSAGE_NAME, JsonUtility.ToJson(rpc), reliable);

                BroadcastNetworkMessage(message, true);
            }

            public void StartGame(object jsonData = null)
            {
                if (!IsHost())
                {
                    Debug.LogError("Unable to StartGame. You are not the host!");
                    return;
                }

                List<string> playerIds = new List<string>();
                foreach (var player in _players)
                    playerIds.Add(player.Id);

                StartGameMessage startGameMessage = new StartGameMessage(playerIds, JsonUtility.ToJson(jsonData));
                NetworkMessage message = new NetworkMessage(STARTGAME_MESSAGE_NAME, JsonUtility.ToJson(startGameMessage), true);
                BroadcastNetworkMessage(message, false);
            }

            public void SendNetworkMessage(string toPlayerId, string messageName, object messageData, bool reliable)
            {
                string jsonData = (string)((messageData as string) != null ? messageData : JsonUtility.ToJson(messageData));
                NetworkMessage message = new NetworkMessage(messageName, jsonData, reliable);
                SendNetworkMessage(toPlayerId, message);
            }

            public void SendNetworkMessage(string toPlayerId, NetworkMessage message)
            {
                _messageQueue.Enqueue(new NetworkMessageQueueEntry(message, toPlayerId));
            }

            public void BroadcastNetworkMessage(NetworkMessage message, bool exceptMyself)
            {
                foreach (var kvp in PlayroomKit.GetPlayers())
                {
                    if (exceptMyself && kvp.Value == PlayroomKit.MyPlayer())
                        continue;

                    SendNetworkMessage(kvp.Key, message);
                }
            }

            public void UpdateNetworkObjectState(NetworkObjectBase networkObject, bool reliable)
            {
                if (!IsInitialized)
                    return;

                if (_networkObjects.Contains(networkObject))
                {
                    UpdateNetworkObjectStateInternal(networkObject, reliable);
                }
            }

            private void OnLocalPlayerDisconnect()
            {
                Debug.Log("OnLocalPlayerDisconnect");
                _isDisconnecting = true;
                Deinitialize();
            }

            private void SetStartTime(float time)
            {
                _startTime = time;
            }

            private void ExitPlayer(string id)
            {
                NetworkPlayer netPlayer = _players.Find(x => x.Id == id);
                _players.Remove(netPlayer);

                _reliableMessages.Remove(id);
                _lastAckedMessageId.Remove(id);

                OnPlayerLeave?.Invoke(netPlayer);
            }

            private void SendSpawnObjectMessage(string objectId, int prefabIndex, Vector3 position, Quaternion rotation, bool exceptMyself)
            {
                //  prefabIndex == -1 means this object was in the scene at the scene start.
                if (prefabIndex < -1 || prefabIndex >= _prefabs.Count)
                    return;

                SpawnObjectMessage item = new SpawnObjectMessage(PlayroomKit.MyPlayer().id, prefabIndex, objectId, position, rotation);

                NetworkMessage message = new NetworkMessage(SPAWN_MESSAGE_NAME, JsonUtility.ToJson(item), true);
                BroadcastNetworkMessage(message, exceptMyself);
            }

            private void SendUnspawnObjectMessage(string id, bool exceptMyself)
            {
                UnspawnObjectMessage item = new UnspawnObjectMessage(id);
                NetworkMessage message = new NetworkMessage(UNSPAWN_MESSAGE_NAME, JsonUtility.ToJson(item), true);
                BroadcastNetworkMessage(message, exceptMyself);
            }

            private void UpdateNetworkObjects()
            {
                foreach (NetworkObjectBase obj in _networkObjects)
                {
                    UpdateNetworkObjectStateInternal(obj, false);
                }
            }

            private void UpdateNetworkObjectStateInternal(NetworkObjectBase networkObject, bool reliable)
            {
                //  Only setState of network objects if has passed enough time
                bool canUpdateState = _updatesPerSecond < 0;
                _deltaTimeAccumulated += UnityEngine.Time.deltaTime;
                float updateDelay = 1f / _updatesPerSecond;
                if (_deltaTimeAccumulated >= updateDelay)
                {
                    int times = Mathf.FloorToInt(_deltaTimeAccumulated / updateDelay);
                    if (times > 0)
                        canUpdateState = true;

                    _deltaTimeAccumulated -= updateDelay * times;
                }

                if (networkObject.IsOwnedByMe())
                {
                    if (!IsHost() || canUpdateState)
                    {
                        if (networkObject.EnabledDataStream)
                        {
                            //  Get current state
                            networkObject.DataStream.Clear();
                            networkObject.GetState().Serialize(networkObject.DataStream);
                            ObjectDataArray currDataArray = networkObject.DataStream.Encode();

                            //  Send current state
                            string json = JsonConvert.SerializeObject(currDataArray);
                            PlayroomKit.SetState("state." + networkObject.Id, json, reliable);
                        }
                        else
                        {
                            //  Get current state
                            string json = JsonUtility.ToJson(networkObject.GetState());

                            //  Send current state
                            PlayroomKit.SetState("state." + networkObject.Id, json, reliable);
                        }
                    }
                }
                else
                {
                    string json = PlayroomKit.GetState<string>("state." + networkObject.Id);
                    if (string.IsNullOrEmpty(json))
                        return;

                    if (networkObject.EnabledDataStream)
                    {
                        ObjectDataArray data = JsonConvert.DeserializeObject<ObjectDataArray>(json);
                        networkObject.DataStream.Clear();
                        networkObject.DataStream.Decode(data);
                        NetworkState state = Activator.CreateInstance(networkObject.GetStateType()) as NetworkState;
                        state.Deserialize(networkObject.DataStream);
                        networkObject.SetState(state);
                    }
                    else
                    {
                        networkObject.SetState(JsonUtility.FromJson(json, networkObject.GetStateType()) as NetworkState);
                    }
                }
            }

            private void ClearNetworkObjectState(NetworkObjectBase networkObject)
            {
                if (!networkObject.IsOwnedByMe())
                    return;

                string[] keysToExclude = new string[] { "state." + networkObject.Id };
                PlayroomKit.ResetStates(keysToExclude);
            }

            private void UpdateMessageQueue()
            {
                if (!IsInitialized)
                    return;
                
                //  Collect a not-acked-reliable-message list for each player
                Dictionary<string, NetworkMessageList> messageListDict = new Dictionary<string, NetworkMessageList>();
                foreach (var kvp in PlayroomKit.GetPlayers())
                {
                    NetworkMessageList notAckedMessageList;
                    if (!_reliableMessages.TryGetValue(kvp.Key, out notAckedMessageList))
                    {
                        notAckedMessageList = new NetworkMessageList();
                        _reliableMessages[kvp.Key] = notAckedMessageList;
                    }
                    NetworkMessageList copy = new NetworkMessageList();
                    copy.List.AddRange(notAckedMessageList.List);

                    messageListDict.Add(kvp.Key, copy);
                }

                while (_messageQueue.Count > 0)
                {
                    //  Add queued messages to the message list of that receiver
                    NetworkMessageQueueEntry entry = _messageQueue.Dequeue();

                    if (!messageListDict.ContainsKey(entry.ReceiverId))
                    {
                        continue;
                    }

                    NetworkMessageList messageList = messageListDict[entry.ReceiverId];
                    messageList.List.Add(entry.Message);
                    
                    //  Add reliable message to the list for the next updates
                    if (entry.Message.Reliable)
                    {
                        if(_reliableMessages.ContainsKey(entry.ReceiverId))
                            _reliableMessages[entry.ReceiverId].List.Add(entry.Message);
                    }
                }
                
                //  Send the message list for each player
                foreach (var kvp in messageListDict)
                {
                    PlayroomKit.Player receiver;
                    if (PlayroomKit.Players.TryGetValue(kvp.Key, out receiver))
                    {
                        string prevJson = receiver.GetState<string>(MESSAGE_KEY + PlayroomKit.MyPlayer().id);
                        string currJson = JsonUtility.ToJson(kvp.Value);
                        if (string.Compare(prevJson, currJson) != 0)
                        {
                            Debug.Log("Sending " + kvp.Value.List.Count + " messages to " + receiver.id);
                            receiver.SetState(MESSAGE_KEY + PlayroomKit.MyPlayer().id, JsonUtility.ToJson(kvp.Value), true);
                        }
                    }
                }
            }

            private void EraseAckedNetworkMessages()
            {
                PlayroomKit.Player myPlayer = PlayroomKit.MyPlayer();

                foreach (var kvp in _reliableMessages)
                {
                    for (int i = kvp.Value.List.Count - 1; i >= 0; --i)
                    {
                        NetworkMessage message = kvp.Value.List[i];
                        PlayroomKit.Player player;
                        if (PlayroomKit.Players.TryGetValue(kvp.Key, out player))
                        {
                            int ackId = player.GetState<int>(ACK_MESSAGE_KEY + myPlayer.id + message.Name);
                            if (ackId == -1)
                            {
                                continue;
                            }

                            if (Utils.LessEqual(message.Id, ackId))
                            {
                                kvp.Value.List.RemoveAt(i);

                                if (kvp.Value.List.Count == 0)
                                {
                                    Debug.Log(message.Name + " Reliable Message List is clear for playerId " + kvp.Key);
                                }
                            }
                        }
                    }
                }
            }

            private void CheckReceivedNetworkMessages()
            {
                PlayroomKit.Player myPlayer = PlayroomKit.MyPlayer();

                foreach (var kvp in PlayroomKit.GetPlayers())
                {
                    string senderId = kvp.Key;
                    string json = myPlayer.GetState<string>(MESSAGE_KEY + senderId);

                    if (string.IsNullOrEmpty(json))
                        return;

                    //  Get the messageList my player received from sender (senderId)
                    NetworkMessageList messageList = JsonUtility.FromJson<NetworkMessageList>(json);

                    if (messageList == null || messageList.List == null)
                        return;

                    List<NetworkMessage> items = messageList.List;

                    int lastAckedMessageId;
                    if (!_lastAckedMessageId.TryGetValue(senderId, out lastAckedMessageId))
                    {
                        lastAckedMessageId = -1;
                    }

                    foreach (NetworkMessage item in items)
                    {
                        if (Utils.LessEqual(item.Id, lastAckedMessageId))
                        {
                            Debug.Log("the message is already handled");
                            continue;
                        }
                        Debug.Log("handle message " + item.Name);
                        try
                        {
                            HandleReceivedNetworkMessage(item);
                        }
                        catch (Exception e)
                        {
                            Debug.LogError("Catch error while handling NetworkMessage " + item.Name);
                        }

                        //  Ack
                        lastAckedMessageId = item.Id;
                        _lastAckedMessageId[senderId] = lastAckedMessageId;
                        myPlayer.SetState(ACK_MESSAGE_KEY + senderId + item.Name, lastAckedMessageId, true);
                        Debug.Log("Acked: " + ACK_MESSAGE_KEY + senderId + item.Name);
                    }
                }
            }

            private void HandleReceivedNetworkMessage(NetworkMessage message)
            {
                if (message.Name == SPAWN_MESSAGE_NAME)
                {
                    HandleSpawnObjectMessage(message);
                }
                else if (message.Name == UNSPAWN_MESSAGE_NAME)
                {
                    HandleUnspawnObjectMessage(message);
                }
                else if (message.Name == RPC_MESSAGE_NAME)
                {
                    HandleRpcMessage(message);
                }
                else if (message.Name == STARTGAME_MESSAGE_NAME)
                {
                    HandleStartGameMessage(message);
                }
                else if (message.Name == KICK_MESSAGE_NAME)
                {
                    HandleKickMessage(message);
                }
                //  TODO
                //  Add more message handlers
                else
                {
                    OnReceiveNetworkMessage?.Invoke(message);
                }
            }

            private void HandleSpawnObjectMessage(NetworkMessage message)
            {
                SpawnObjectMessage item = JsonUtility.FromJson<SpawnObjectMessage>(message.JsonData);
                NetworkObjectBase netObj = null;
                if (item.PrefabIndex == -1)
                {
                    //  The object already exists, but is unidentified
                    if (_unidentifiedNetworkObjects.TryGetValue(item.ObjectId, out netObj))
                    {
                        _unidentifiedNetworkObjects.Remove(item.ObjectId);
                        AddNetworkObject(netObj);
                    }
                }
                else
                {

                    //  Actually spawn the object
                    GameObject prefab = _prefabs[item.PrefabIndex];
                    GameObject go = Instantiate(prefab, item.Position, Quaternion.Euler(item.Rotation));
                    netObj = go.GetComponent<NetworkObjectBase>();
                }

                //  Initialize the network object
                if (netObj != null)
                    netObj.Initialize(item.ObjectId, netObj.Owner == NetworkOwner.LocalPlayer && item.SenderId != PlayroomKit.MyPlayer().id);
            }

            private void HandleUnspawnObjectMessage(NetworkMessage message)
            {
                UnspawnObjectMessage item = JsonUtility.FromJson<UnspawnObjectMessage>(message.JsonData);
                NetworkObjectBase netObj = _networkObjects.Find(x => x.Id == item.ObjectId);

                if (netObj == null)
                {
                    Debug.LogWarning("Unable to unspawn object '" + item.ObjectId + "'. Object not found!");
                    return;
                }

                ClearNetworkObjectState(netObj);
                Destroy(netObj.gameObject);
                _networkObjects.Remove(netObj);
            }

            private void HandleRpcMessage(NetworkMessage message)
            {
                RpcMessage rpc = JsonUtility.FromJson<RpcMessage>(message.JsonData);
                NetworkObjectBase netObj = _networkObjects.Find(x => x.Id == rpc.ObjectId);
                if (netObj == null)
                    return;

                netObj.gameObject.SendMessage(rpc.MethodName, rpc.JsonData);
            }

            private void HandleStartGameMessage(NetworkMessage message)
            {
                StartGameMessage startGameMessage = JsonUtility.FromJson<StartGameMessage>(message.JsonData);
                for (int i = 0; i < startGameMessage.PlayerIds.Count; i++)
                {
                    NetworkPlayer player = _players.Find(x => x.Id == startGameMessage.PlayerIds[i]);
                    player.Index = i;
                }

                OnStartGame?.Invoke(startGameMessage);
            }

            private void HandleKickMessage(NetworkMessage message)
            {
                if (IsHost())
                {
                    string playerId = message.GetData<string>();
                    PlayroomKit.GetPlayer(playerId).Kick();
                }
            }
        }
    }
}