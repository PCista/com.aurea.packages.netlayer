using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {

        [System.Serializable]
        public class NetworkState
        {
            public virtual void Serialize(DataStream dataStream) { }
            public virtual void Deserialize(DataStream dataStream) { }
        }
    }
}