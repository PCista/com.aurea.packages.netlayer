using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {
        [System.Serializable]
        public class NetworkMessage
        {
            private static int NextId = 0;

            public string Name = "";
            public string JsonData = "{}";
            public bool Reliable = false;
            public int Id = -1;

            public NetworkMessage(string name, string jsonData, bool reliable)
            {
                Name = name;
                JsonData = jsonData;
                Reliable = reliable;
                Id = NextId;

                NextId++;
                if (NextId == -1)
                    NextId++;
            }

            public T GetData<T>()
            {
                if (typeof(T) == typeof(string))
                    return (T)(object)JsonData;

                return JsonUtility.FromJson<T>(JsonData);
            }

            public void SetData(object data)
            {
                if (data is string)
                    JsonData = (string)data;
                else
                    JsonData = JsonUtility.ToJson(data);
            }
        }


        /// <summary>
        /// Helper class to allow JsonUtility to serialize a list of NetworkMessage
        /// </summary>
        [System.Serializable]
        public class NetworkMessageList
        {
            public List<NetworkMessage> List = new List<NetworkMessage>();
        }
    }
}