using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {
        [System.Serializable]
        public class SpawnObjectMessage
        {
            public string SenderId;
            public int PrefabIndex;
            public string ObjectId;
            public float Time;

            [SerializeField] private Vector3Int _position;
            [SerializeField] private Vector3Int _rotation;

            public Vector3 Position => Utils.DequantizeVector3(_position);
            public Vector3 Rotation => Utils.DequantizeVector3(_rotation);

            public SpawnObjectMessage(string senderId, int prefabIndex, string objectId, Vector3 position, Quaternion rotation)
            {
                SenderId = senderId;
                PrefabIndex = prefabIndex;
                ObjectId = objectId;
                Time = NetworkManager.Time;
                _position = Utils.QuantizeVector3(position);
                _rotation = Utils.QuantizeVector3(rotation.eulerAngles);
            }
        }
    }
}