using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {

        [System.Serializable]
        public class StartGameMessage
        {
            /// <summary>
            /// A list of playerIds sorted as they are in the Host
            /// At the start of the game, we need to know the player order. 
            /// So we send a list of playerIds, so that each player knows its own index and each other indexes too
            /// </summary>
            public List<string> PlayerIds;
            public float StartTime;
            public string JsonData;

            public StartGameMessage(List<string> playerIds, string jsonData)
            {
                StartTime = NetworkManager.Time;
                PlayerIds = playerIds;
                JsonData = jsonData;
            }
        }
    }
}