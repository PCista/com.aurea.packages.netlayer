using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {

        [System.Serializable]
        public class RpcMessage
        {
            public string ObjectId;
            public string MethodName;
            public string JsonData;
        }
    }
}