using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurea
{
    namespace NetLayer
    {
        [System.Serializable]
        public class UnspawnObjectMessage
        {
            public string ObjectId;

            public UnspawnObjectMessage(string objectId)
            {
                ObjectId = objectId;
            }
        }
    }
}