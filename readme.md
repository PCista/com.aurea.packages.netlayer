# Aurea NetLayer (0.2.0)

This project is a Network Layer for games that use PlayroomKit as a network SDK.
It aims to be a Network Layer for any network SDK in the future, however it is still a **WIP**. 
It aims to be a community driven project. So if your have a request or any type of contribution, feel free to submit a request.


## Features:
- Generic classes for synching any type of objects
- Sample showing how to sync position and rotations of objects over the network (supports interpolation)
- Different object authorities (Host and LocalPlayer)
- Spawn/Unspawn objects over the network
- Send Messages to others (including or excluding yourself)
- RPC (...kind of). It uses Unity message system.


## Setup:
- Import this package (located in folder `Assets/Aurea/NetLayer/Package`) into your Unity project or copy paste the files into it.
- After that, on Unity go to Windows->Package Manager, once the Package Manager window opens, go to Add package from git URL, type com.unity.nuget.newtonsoft-json press Add and done.
- This Asset uses PlayroomPlugin v0.0.14. It should work fine with more recent versions, but it was not tested.

## How to use:

### TLDR;
Here are the basic steps:
- Inherit class `NetworkObjectBase` and add the component to every object you need to sync
- Add a NetworkManager to the scene and configure it appropriatedly.
- Register callbacks to the `NetworkManager` events before initializing it.
- Inside `OnPlayerJoin` callback, spawn the player object and call `Initialize` with a unique id for each player.
- Call `NetworkManager.Instance.Initialize()` to start the Playroom lobby.
- Call `NetworkManager.Instance.StartGame()` to start the game and get player indexes.
- Call `NetworkManager` methods whenever you need to send a message, spawn/unspawn objects, call rpcs, etc.

### Initializing the NetworkManager:
- Add a NetworkManager object to the scene and configure it as you want.
- Register callback for methods in the `NetworkManager` object before initializing it.
- Call `NetworkManager.Instance.Initialize()` to start the Playroom lobby.
- After all players joined, call `NetworkManager.Instance.StartGame()`, to receive a callback of start game.

```
private void Start()
{
    NetworkManager.Instance.OnPlayerJoin += OnPlayerJoin;
    NetworkManager.Instance.OnStartGame += OnStartGame;
    NetworkManager.Instance.Initialize();
}

private void OnPlayerJoin(Aurea.NetLayer.NetworkPlayer netPlayer)
{
	//	Start the game when there are 2 players. Only the host can start the game.
	if (NetworkManager.Instance.GetPlayers().Count == 2 && NetworkManager.Instance.IsHost())
    {
        NetworkManager.Instance.StartGame();
    }
}

private void OnStartGame(Aurea.NetLayer.StartGameMessage message)
{
	//	Start game here
}
``` 

### Sync objects over the network
Any object that has to be synched needs to have a component that inherits the `NetworkObjectBase` component. That component will needs to implement `GetState` and `SetState` methods in order to sync. See the sample included for more details.

To spawn/unspawn an object and replicate this over the network, you must include the object prefab in the prefab list of the `NetworkManager` the call the methods `SpawnObject/UnspawnObject` of it.

### Spawning objects
There are 3 ways of spawning objects that are synchec over the network.
In each case, the object needs to have a component that inherits `NetworkObjectBase` and implement its `GetState/SetState` methods.
1 - **Adding the object to the scene:** You can add an network object inside your scene while creating it. In this case,  NetLayer uses the path of that object, in the scene hierarchy, as it's Id. So make sure that every network object you add to your scene has a different path in the scene. Also, make sure to set the object Owner to `Host`.
2 - **Spawning at runtime:** You can easily spawn an object at runtime and replicated that over the network. To do that, first add the object prefab to the `NetworkManager` list of prefabs. Set the prefab owner to either `Host` or `LocalPlayer`. In this case, NetLayer created a unique Id for that object automatically.
3 - **Spawning at runtime locally to each player:** If you prefer, you can Spawn the object locally to each player than let NetLayer sync them after other players have spawned them too. This can be usefull in cases you know a method is going to be called for every player, like the `OnPlayerJoin` callback, that is called on every player whenever a new player join the room. If the object Owner is supposed to be `LocalPlayer`, make sure that for other players it is set to `Other`. See more in *Instantiating the Players*.

### Instantiating the players
Although you can instantiate your player GameObject in the `OnPlayerJoin` callback, you probably want to do that in the `OnStartGame` callback. That is because when `OnPlayerJoin` is called, players don't have their indexes set yet (do not confuse with Id, at this time, they already have an Id but don't have an Index). So if your game needs to spawn every player in a specific position based on their index, you should do that in the `OnStartGame` callback. 
**[IMPORTANT]** Players must:
- Have a <ins>NetworkObjectBase</ins> component
- Have owner property set to <ins>LocalPlayer</ins>
- Be <ins>spawned in runtime</ins>, (not included in scene file).
- Call <ins>Initialize</ins> right after being spawned, with a <ins>different unique id</ins> (you can use the playerId for that)

```
[SerializeField] private Transform[] _spawnPositions;
[SerializeField] private GameObject _playerPrefab;

private void OnStartGame(float startTime)
{
    foreach (PlayerConfig config in _playerConfigs)
    {
        int playerIndex = NetworkManager.Instance.GetPlayer(config.PlayerId).Index;
        bool isLocalPlayer = NetworkManager.Instance.GetPlayer(config.PlayerId).IsLocal;
        
        Transform spawnPosition = _spawnPositions[playerIndex];
        GameObject go = Instantiate(_playerPrefab, spawnPosition.position, spawnPosition.rotation);
        go.GetComponent<NetworkObject>().Initialize("Player_" + config.PlayerId, !isLocalPlayer);
    }
}
```

### RPC
```
NetworkManager.Instance.Rpc(GetComponent<NetworkObject>(), "MyMethodName", null, true);
```
The RPC method, must be public and be writen in a component attached to the same gameobject that has the `NetworkObject` component.

### Sending Custom Messages
```
NetworkManager.Instance.SendNetworkMessage(toPlayerId, "MyMessage", messageData, reliable);
```
`messageData` can be any object with [System.Serializable] attribute

### Recieving Custom Messages
```
private void Awake()
{
	NetworkManager.Instance.OnReceiveNetworkMessage += OnNetworkMessage;
}

private void OnNetworkMessage(NetworkMessage message)
{
	MyMessageData data = JsonUtility.FromJson<MyMessageData>(message.JsonData);
	//	handle the message
}
```

## WebGL Demo:
Take a look at the [WegGL Demo](https://cloud.unity.com/public/build-automation/share?shareId=BoV8StxQnpXz0BF6_Pxzfav4CH1xo6W7PDsbow1O4nw) and test by yourself.

## WIP:
This is a work-in-progress project. There are many thing we still plan to do. Here is a list of future changes:
- Abstraction of NetworkManager. Currently, the class NetworkManager only works with PlayroomKit, that means it only supports WebGL games. In order to make it more usable for other platforms, The NetworkManager class should be abstracted, so that different multiplayer SDKs can be used depending on the platform you are building to.
- Better lobby experience. The current Playroom lobby experience works well, but you probably want to make your own user interface. In order to do that, we are going to provide classes and methods that help.